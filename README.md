# Google Cloud Functions


* [Google Cloud Functions Documentation](https://cloud.google.com/functions/docs/)
* [Quickstart](https://cloud.google.com/functions/docs/quickstart)


## Node.js dev env setup

* [Setting Up a Node.js Development Environment](https://cloud.google.com/nodejs/docs/setup)
* [Google Cloud Client Library for Node.js](https://googlecloudplatform.github.io/google-cloud-node/#/)


Install google-cloud npm packages:

    npm install --save google-cloud

Or,

    npm install --save @google-cloud/storage
    ...


